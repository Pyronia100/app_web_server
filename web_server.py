from flask import request, url_for, jsonify, Response
from flask_cors import CORS
from flask_api import FlaskAPI, status, exceptions
import get_information
import json
from sseclient import SSEClient

app = FlaskAPI(__name__)
CORS(app)

slugs = ['/database-status','/login','/average-prices','/ending-position','/realized-profit','/effective-profit']

@app.route(slugs[0])
def info0():
    return jsonify(get_information.get_info(slugs[0]))

@app.route(slugs[1])
def info1():
    return jsonify(get_information.get_info(slugs[1]))

@app.route(slugs[2])
def info2():
    return jsonify(get_information.get_info(slugs[2]))

@app.route(slugs[3])
def info3():
    return jsonify(get_information.get_info(slugs[3]))

@app.route(slugs[4])
def info4():
    return jsonify(get_information.get_info(slugs[4]))

@app.route(slugs[5])
def info5():
        #print(get_information.get_info(slugs[5]))
        return jsonify(get_information.get_info(slugs[5]))


@app.route('/deals/stream')
def stream():
    # messages = SSEClient('http://localhost:8080/datastream')
    def eventStream():
        messages = SSEClient('http://dao2-datagenerator.apps.dbgrads-6eec.openshiftworkshop.com/deals/stream')
        print(messages)
        for msg in messages:
            #print(type(msg))
            outputMsg = msg.data
            #print((msg.data))
            #outputJS = jsonify(outputMsg)
    # FilterName = "data"

            # wait for source data to be available, then push it
            yield 'data:' + msg.data.replace("'", '"') +'\n\n'

    return Response(eventStream(), mimetype="text/event-stream")

# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     # user_name = request.args.get("username")
#     # hashed_password = request.args.get("passwordHash")
#     # print (user_name)
#     # print (hashed_password)
#     data = request.get_json()
#     user_name = data['username']
#     password = data['passwordHash']
#     cnx = mysql.connector.connect(host='192.168.99.100',
#                                   user='root', password='ppp',
#                                   database='deal_data')
#     cursor = cnx.cursor(buffered=True)
#     query = "SELECT password, user_role FROM shadow WHERE user_name=\'{}\'".format(user_name)
#     cursor.execute(query)
#     for (data) in cursor:
#         password_hash = int(data[0])
#         user_role = data[1]

#     if password_hash == password:
#         return jsonify([{'username' : user_name, 'role':user_role}])
#     else:
#         return jsonify([{}])

def bootapp():     
        app.run(port=8080, debug=True, threaded=True, host=('0.0.0.0'))





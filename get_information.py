import json
import requests

url = 'http://dao2-datagenerator.apps.dbgrads-6eec.openshiftworkshop.com'
slugs = ['/database-status','/login','/average-prices','/ending-position','/realized-profit','/effective-profit']
#api_url = '{}:5000/{}'.format(url,'database-status')
#print(api_url)

def get_info(slug):
    
    api_url = '{}:8080{}'.format(url,slug)
    #print(api_url)
    response = requests.get(api_url)
    #print(response.json())
    if response.status_code == 200:
        return response.json()
    else:
        return None

#get_info('/ending-position')